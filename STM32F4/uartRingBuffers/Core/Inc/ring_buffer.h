/*
 * ring_buffer.h
 *
 *  Created on: Jul 27, 2023
 *      Author: ivan
 */

#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_

#include <stdlib.h>
#include <stdint.h>
#include <string.h>


// Opaque circular buffer structure
typedef struct circular_buf_t circular_buf_t;

// Handle type, the way users interact with the API
typedef circular_buf_t *cbuf_handle_t;




// Function prototypes
/// Pass in a storage buffer and size
/// Returns a circular buffer handle
cbuf_handle_t ringBufferInit(uint8_t *buffer, uint32_t size, uint32_t packetLength, uint32_t *nElements);

/// Free a circular buffer structure.
/// Does not free data buffer; owner is responsible for that
extern void ringBufferFree(cbuf_handle_t self);

/// Reset the circular buffer to empty, head == tail
extern void ringBufferReset(cbuf_handle_t self);

/// Put version 1 continues to add data if the buffer is full
/// Old data is overwritten
extern void ringBufferPut(cbuf_handle_t self, uint8_t *data, uint32_t thisArrayLen);

/// Put Version 2 rejects new data if the buffer is full
/// Returns 0 on success, -1 if buffer is full
extern int8_t ringBufferPut2(cbuf_handle_t self, uint8_t *data, uint32_t thisArrayLen);

/// Retrieve a value from the buffer
/// Returns the current element length on success, -1 if the buffer is empty
extern int32_t  ringBufferGet(cbuf_handle_t self, uint8_t * data);

/// Returns true if the buffer is empty
extern uint8_t ringBufferEmpty(cbuf_handle_t self);

/// Returns true if the buffer is full
extern uint8_t ringBufferFull(cbuf_handle_t self);

/// Returns the maximum capacity of the buffer
extern uint32_t ringBufferCapcity(cbuf_handle_t self);

/// Returns the current number of elements in the buffer
extern uint32_t ringBufferUsedSlots(cbuf_handle_t self);


#endif /* INC_RING_BUFFER_H_ */
