/*
 * ring_buffer.c
 *
 *  Created on: Jul 27, 2023
 *      Author: ivan
 *
 *  Based on source code from:
 *  https://embeddedartistry.com/blog/2017/05/17/creating-a-circular-buffer-in-c-and-c/
 *
 *  This version is compatible with arrays (packets) as single elements in the ring buffer.
 *  The length of the arrays (must be the same size) is defined in the struct element "packetLength",
 *  which is passed as a parameter in the initialization
 *
 */

#include "ring_buffer.h"


// Private function prototypes
static void advancePointer_(cbuf_handle_t self);
static void retreatPointer_(cbuf_handle_t self);


/*
 * ==========================================
 * Private structure and function definitions
 * ==========================================
 */


/**
 * @brief	Buffer ring structure that contains the data buffer, pointer counters and related metadata
 * @note	Remains private to allow multiple instances from the user
*/
struct circular_buf_t {
	uint8_t  *buffer;
	uint32_t *nElements;
	uint32_t head;
	uint32_t tail;
	uint32_t max; //of the buffer
	uint8_t full;
	uint32_t packetLength; // width of the vector elements in the ring buffer (1 if no array is stored)
};


/**
 * @brief	Private function to advance the ring pointer one location (when data is added)
 * @param	self:	ring buffer handler instance
 */

static void advancePointer_(cbuf_handle_t self)
{

	if(self->full)
   	{
		if (++(self->tail) == self->max)
		{
			self->tail = 0;
		}
	}

	if (++(self->head) == self->max)
	{
		self->head = 0;
	}
	self->full = (self->head == self->tail);
}

/**
 * @brief	Private function to retreat the ring pointer one location (when data is added)
 * @param	self:	ring buffer handler instance
 */

static void retreatPointer_(cbuf_handle_t self)
{

	self->full = 0;
	if (++(self->tail) == self->max)
	{
		self->tail = 0;
	}
}

/*
 * ===========================
 * Public function definitions
 * ===========================
 */

/**
 * @brief	Initialize ring buffer structure with a pre-allocated buffer
 * @note	Malloc for the chosen buffer must be carried out by the user
 * 			such as:
 * 				uint8_t *buffer = malloc(UART_BUFFER_DEPTH * sizeof(uint8_t));
 *
 * 			The buffer is then assigned in the ring buffer structure
 * @param	buffer: pre-allocated buffer pointer on which data will be stored
 * @param	size:	max number of elements to store in the buffer (the size of the ring buffer)
 * @param	packetLength: the number of items contained in each single element. May be one if no arrays are needed
 * @param	nElements: vector where the individual length of each element in the buffer is stored
 * @retval	Handler pointer of the ring buffer instance
 */
cbuf_handle_t ringBufferInit(uint8_t *buffer, uint32_t size, uint32_t packetLength, uint32_t *nElements)
{

	cbuf_handle_t cbuf = malloc(sizeof(circular_buf_t));

	cbuf->buffer = buffer;
	cbuf->nElements = nElements;
	cbuf->max = size;
	cbuf->packetLength = packetLength;
	ringBufferReset(cbuf);

	return cbuf;
}


/**
 * @brief	Resets the ring buffer counters
 * @param	self:	ring buffer handler instance
 */
void ringBufferReset(cbuf_handle_t self)
{
    self->head = 0;
    self->tail = 0;
    self->full = 0;
}


/**
 * @brief	Clears the ring buffer contents (buffer and counters)
 * @param	self:	ring buffer handler instance
 */
void ringBufferFree(cbuf_handle_t self)
{
	free(self);
}

/**
 * @brief	Returns the "full" status of the ring buffer associated to the handler
 * @param	self:	ring buffer handler instance
 * @retval	Returns 1 if the current ring buffer is full, 0 otherwise
 */
uint8_t ringBufferFull(cbuf_handle_t self)
{
	return (uint8_t) ((self->full > 0)?1:0);
}



/**
 * @brief	Returns the "empty" status of the ring buffer associated to the handler
 * @param	self:	ring buffer handler instance
 * @retval	Returns 1 if the current ring buffer is empty, 0 otherwise
 */
uint8_t ringBufferEmpty(cbuf_handle_t self)
{
	return (uint8_t) ((!self->full && (self->head == self->tail))?1:0);
}


/**
 * @brief	Returns the ring buffer maximum capacity
 * @param	self:	ring buffer handler instance
 * @retval	Returns the total number of elements (max) that can be stored in the ring buffer
 */
uint32_t ringBufferCapcity(cbuf_handle_t self)
{
	return self->max;
}


/**
 * @brief	Returns the total number of elements currently stored in the ring buffer
 * @param	self:	ring buffer handler instance
 * @retval	Returns the number of elements currently stored in the ring buffer
 */
uint32_t ringBufferUsedSlots(cbuf_handle_t self)
{

	uint32_t size = self->max;

	if(!self->full)
	{
		if(self->head >= self->tail)
		{
			size = (self->head - self->tail);
		}
		else
		{
			size = (self->max + self->head - self->tail);
		}
	}

	return size;
}


/**
 * @brief	Puts an element into the circular buffer
 * @note	If the buffer is full, the oldest element is replaced
 * @param	self:	ring buffer handler instance
 * @param	data:	new element to be stored in the ring buffer structure
 * @param	thisArrayLen: length of the element to be stored
 */
void ringBufferPut(cbuf_handle_t self, uint8_t *data, uint32_t thisArrayLen)
{
	memcpy(self->buffer+(self->head*self->packetLength), data, self->packetLength);
	memcpy(self->nElements+(self->head), &thisArrayLen, sizeof(uint32_t));
    advancePointer_(self);
}



/**
 * @brief	Puts an element into the circular buffer
 * @note	No replacement of older data is done if the buffer is full
 * @param	self:	ring buffer handler instance
 * @param	data:	new element to be stored in the ring buffer structure
 * @param	thisArrayLen: length of the element to be stored
 * @retval	Returns -1 if the buffer is already full and no element was stored, 0 otherwise
 */
int8_t ringBufferPut2(cbuf_handle_t self, uint8_t *data, uint32_t thisArrayLen)
{
    int r = -1;

    if(!ringBufferFull(self))
    {
    	memcpy(self->buffer+(self->head*self->packetLength), data, self->packetLength);
    	memcpy(self->nElements+(self->head), &thisArrayLen, sizeof(uint32_t));
        advancePointer_(self);
        r = 0;
    }

    return r;
}


/**
 * @brief	Takes the oldest element from the circular buffer structure if not empty
 * @param	self:	ring buffer handler instance
 * @param	*data:	pointer to the destination variable where the element from the pointer will be stored
 * @retval	Returns -1 if the ring buffer was empty and no retrieval was possible; 0 otherwise
 */
int32_t ringBufferGet(cbuf_handle_t self, uint8_t *data)
{

    int32_t arrayLen;

    if(!ringBufferEmpty(self))
    {
        memcpy(data, self->buffer+(self->tail*self->packetLength), self->packetLength);
        memcpy((uint32_t *) &arrayLen, self->nElements+(self->tail), sizeof(uint32_t));
        retreatPointer_(self);

        return (int32_t) arrayLen;

    }

    return -1;
}

