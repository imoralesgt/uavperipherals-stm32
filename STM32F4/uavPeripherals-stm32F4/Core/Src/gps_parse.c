/*
 * gps_parse.c
 *
 *  Created on: Jul 18, 2023
 *      Author: ivan
 *      Note: Based on the work from sztuka (https://github.com/sztvka/stm32-nmea-gps-hal)
 */

#include "gps_parse.h"

char *thisData[NMEA_SENTENCE_BUFFER_LEN];
uint32_t triesBeforeSignalLost = 0;


/**
 * @brief	Converts latitude or longitude from ddmm.mmmm format to degrees in 1E7 format
 * @param	degrees				lat or lon in degrees format as parsed from GGA sentence
 * @param	minutes				lat or lon minutes with decimal values as parsed from GGA sentence
 * @param	side				Expects -1 for South or West and +1 for North or East
 * @retval	Signed integer with the latitude or longitude in 1E7 format
 */

int32_t ddmmmmToDecimalDegrees(uint8_t degrees, float minutes, int8_t side){
	double totalDegrees;
	totalDegrees = degrees + minutes / 60.0;
	totalDegrees *= 10000000; //1E7 format

	if((side == 1) || (side == -1))
		totalDegrees *= side;

	return (int32_t) totalDegrees;
}

/**
 * @brief	Converts knots to m/s using the exact definitions (no approximations)
 * @param	knots	Speed over ground in knots
 * @retval	Speed over ground in meters/second
 */
float knotsToMetersSecond(float knots){
	const float KM_H_PER_KNOT = 1.852;
	const float M_S_PER_KM_H = 1/3.6;

	return knots*KM_H_PER_KNOT*M_S_PER_KM_H;
}



/**
 * @brief	Verifies GPS data validity by checking the provided data and checksum
 * @param	nmeaData	Received NMEA sentence
 * @retval	int			Checksum valid = 1, otherwise = 0
 */

int nmeaChecksum(char *nmeaData){
    //if you point a string with less than 5 characters the function will read outside of scope and crash the mcu.
    if(strlen(nmeaData) < 5) return 0;
    char recvCrc[2];
    recvCrc[0] = nmeaData[strlen(nmeaData) - 4];
    recvCrc[1] = nmeaData[strlen(nmeaData) - 3];
    int crc = 0;
    int i;

    //exclude the CRLF plus CRC with an * from the end
    for (i = 0; i < strlen(nmeaData) - 5; i ++) {
        crc ^= nmeaData[i];
    }
    int receivedHash = strtol(recvCrc, NULL, 16);
    if (crc == receivedHash) {
        return 1;
    }
    else{
        return 0;
    }
}


/**
 *  @brief	Parses the received NMEA GGA sentence and stores the values to the destination GPS struct
 *  @note	Looks for the following data within the sentence:
 *  			- GPS Fix	(uint8_t)
 *  			- Number of satellites used to fix (uint8_t)
 *  			- Latitude (needs conversion to E7 uint32_t format)
 *  			- Longitude (needs conversion to E7 uint32_t format)
 *  			- Altitude (in meters over sea level units) - float
 *  			- HDOP (horizontal position error) - float
 *  @param	gpsData				pointer to destination GPS data struct
 *  @param	inputString			pointer to input string received from UART (already individualized as GGA sentence)
 *  @retval	HAL_StatusTypeDef	HAL_OK or HAL_ERROR
 */
HAL_StatusTypeDef nmeaGGA(GPS *gpsData, char *inputString){
	static uint8_t fixQuality = 0;
	static int32_t latitudeE7;
	static int32_t longitudeE7;
	static uint8_t satelliteCnt;
	static float altitude;
	static float hdop;

	uint8_t latDeg;
	uint8_t lonDeg;

	float latMin;
	float lonMin;


	HAL_StatusTypeDef retVal = HAL_OK;

	char *values[25];
	int counter = 0;
	memset(values, 0, sizeof(values));
	char *marker = strtok(inputString, ",");
	while (marker != NULL){
		values[counter++] = malloc(strlen(marker) + 1);
		strcpy(values[counter - 1], marker);
		marker = strtok(NULL, ",");
	}

	char latSide = values[3][0]; //North or south
	char lonSide = values[5][0]; //East or west


	strcpy(gpsData -> lastMeasure, values[1]);

	// Parsing satellite fix
	fixQuality = (uint8_t) strtol(values[6], NULL, 10);
	fixQuality = fixQuality > 2 ? 0 : fixQuality;

	// Parsing latitude
	if(latSide == 'N' || latSide == 'S'){ //If parsing was correct
		char lat_d[2];
		char lat_m[7];

		int8_t latSideSign = latSide == 'S' ? -1 : latSide == 'N' ? 1 : 0;

		for(uint8_t z = 0; z < 2; z++)	lat_d[z] = values[2][z];
		for(uint8_t z = 0; z < 7; z++)	lat_m[z] = values[2][z + 2]; //Note: Original code looped until z<6. Check this!

		latDeg = (uint8_t) strtol(lat_d, NULL, 10);
		latMin = strtof(lat_m, NULL);

		// Compute latitude in 1E7 format
		latitudeE7 = ddmmmmToDecimalDegrees(latDeg, latMin, latSideSign);

	}else{
		retVal = HAL_ERROR;
	}

	// Parsing longitude
	if(lonSide == 'W' || lonSide == 'E'){
		char lon_d[3];
		char lon_m[7];

		int8_t lonSideSign = lonSide == 'W' ? -1 : lonSide == 'E' ? 1 : 0;

		for(uint8_t z = 0; z < 3; z++)	lon_d[z] = values[4][z];
		for(uint8_t z = 0; z < 7; z++)	lon_m[z] = values[4][z + 3]; //Note: Original code looped until z<6. Check this!

		lonDeg = (uint8_t) strtol(lon_d, NULL, 10);
		lonMin = strtof(lon_m, NULL);

		// Compute longitude in 1E7 format
		longitudeE7 = ddmmmmToDecimalDegrees(lonDeg, lonMin, lonSideSign);

	}else{
		retVal = HAL_ERROR;
	}

	// Parsing altitude
	altitude = strtof(values[9], NULL);
	altitude = altitude != 0 ? altitude : gpsData -> altitude;

	// Parsing satellite count
	satelliteCnt = (uint8_t) strtol(values[7], NULL, 10);

	// Parsing HDOP
	hdop = strtof(values[8], NULL);
	hdop = hdop != 0 ? hdop : gpsData -> hdop;


	//Check the parsed data consistency before assignment to GPS struct
	if(latDeg != 0 && lonDeg != 0 && latDeg < 90 && lonDeg < 180){
		gpsData -> fix = fixQuality;
		gpsData -> satelliteCount = satelliteCnt;
		gpsData -> latE7 = latitudeE7;
		gpsData -> lonE7 = longitudeE7;
		gpsData -> altitude = altitude;
		gpsData -> hdop = hdop;
	}else{
		retVal = HAL_ERROR;
	}

	// Free up reserved memory for NMEA sentence
	for(int i = 0; i < counter; i++)
		free(values[i]);


	return retVal;
}


/**
 *  @brief	Parses the received NMEA RMC sentence and stores the values to the destination GPS struct
 *  @note	Looks for the following data within the sentence:
 *  			- UTC Time	(uint32_t)
 *  			- UTC Date	(uint32_t)
 *  			- Course over ground (in 0.01 degrees units) - float
 *  			- Ground speed (needs conversion from knots to m/s) - float
 *  @param	gpsData				pointer to destination GPS data struct
 *  @param	inputString			pointer to input string received from UART (already individualized as RMC sentence)
 *  @retval	HAL_StatusTypeDef	HAL_OK or HAL_ERROR
 */
HAL_StatusTypeDef nmeaRMC(GPS *gpsData, char *inputString){
	HAL_StatusTypeDef retVal = HAL_OK;

	char *values[25];
	int counter = 0;
	memset(values, 0, sizeof(values));
	char *marker = strtok(inputString, ",");

	while(marker != NULL){
		values[counter++] = malloc(strlen(marker) + 1);
		strcpy(values[counter - 1], marker);
		marker = strtok(NULL, ",");
	}

	uint8_t gpsValid = 0;
	uint32_t utcTime = 0;
	uint32_t utcDate = 0;
	float courseOvrGnd = 0.00;
	float groundSpeed = 0.0;


	// Parse and check GPS data valid
	gpsValid = values[2][0] == 'A' ? 1 : 0;
	gpsData -> valid = gpsValid;

	// If the current GPS data is valid, update the GPS struct
	if(gpsValid){

		// Parsing UTC Time (HHMMSS format) - we remove miliseconds here
		char utcTimeChr[6];
		for(uint8_t z = 0; z < sizeof(utcTimeChr); z++)		utcTimeChr[z] = values[1][z];
		utcTime = (uint32_t) strtol(utcTimeChr, NULL, 10);

		// Parsing UTC Date (DDMMYY format)
		utcDate = (uint32_t) strtol(values[9], NULL, 10);

		// Parsing Course Over Ground (0.01 deg resolution - float)
		courseOvrGnd = strtof(values[8], NULL);

		// Parsing Ground Speed - we convert knots to m/s here
		groundSpeed = knotsToMetersSecond(strtof(values[7], NULL));


		// Since GPS data is valid, let's update the GPS struct
		gpsData -> utcTime = utcTime;
		gpsData -> utcDate = utcDate;
		gpsData -> courseGround = courseOvrGnd;
		gpsData -> groundSpeed = groundSpeed;

	}

	// Free up reserved memory for NMEA sentence
	for(int i = 0; i < counter; i++)
		free(values[i]);


	return retVal;
}




/**
 *  @brief	Parses the received NMEA GLL sentence and stores the values to the destination GPS struct
 *  @note	Looks for the following data within the sentence:
 *  			- GPS Valid	(uint8_t)
 *  @param	gpsData				pointer to destination GPS data struct
 *  @param	inputString			pointer to input string received from UART (already individualized as GLL sentence)
 *  @retval	HAL_StatusTypeDef	HAL_OK or HAL_ERROR
 */
HAL_StatusTypeDef nmeaGLL(GPS *gpsData, char *inputString){
	char *values[25];
	int counter = 0;
	memset(values, 0, sizeof(values));
	char *marker = strtok(inputString, ",");
	while(marker != NULL){
		values[counter++] = malloc(strlen(marker) + 1);
		strcpy(values[counter - 1], marker);
		marker = strtok(NULL, ",");
	}

	static uint8_t gpsValid = 0;
	gpsValid = values[6][0] == 'A' ? 1 : 0;

	gpsData -> valid = gpsValid;

	// Free up reserved memory for NMEA sentence
	for(int i = 0; i < counter; i++)
		free(values[i]);

	return HAL_OK;
}



/**
 * @brief	Parses the received char buffer until the NMEA tokens are found.
 * @note	Individual NMEA sentences are individualized and processed by auxiliary parsing functions
 * @param	gpsData		pointer to structure where the GPS parsed data will be stored
 * @param	buffer		pointer to received raw data buffer from the UART interface
 * @param	HAL_StatusTypeDef	HAL_OK or HAL_ERROR
 */
HAL_StatusTypeDef nmeaParse(GPS *gpsData, uint8_t *buffer){
	memset(thisData, 0, sizeof(thisData));
	char *token = strtok(buffer, "$");
	int cnt = 0;
	triesBeforeSignalLost = triesBeforeSignalLost <= TRIES_BEFORE_SIGNAL_LOST ? triesBeforeSignalLost + 1 : TRIES_BEFORE_SIGNAL_LOST;
	while(token != NULL && cnt < NMEA_SENTENCE_BUFFER_LEN){ //The number of tokens found
		thisData[cnt++] = malloc(strlen(token) + 1);
		strcpy(thisData[cnt - 1], token);
		token = strtok(NULL, "$");
	}

	// Process the NMEA sentences found with the token search. Only seek GGA and RMC sentences.
	for(int i = 0; i < cnt; i++){
		if((strstr(thisData[i], "\r\n") != NULL) && (nmeaChecksum(thisData[i]))){
			if(strstr(thisData[i], "GGA") != NULL){ // If a GGA sentence is detected, parse it
				nmeaGGA(gpsData, thisData[i]);
				triesBeforeSignalLost = 0;
			}
			else if(strstr(thisData[i], "RMC") != NULL){ // If a RMC sentence is detected, parse it
				nmeaRMC(gpsData, thisData[i]);
				triesBeforeSignalLost = 0;
			}

		}
	}

	// In case the GPS gets unplugged or loses power, the fix is lost and must be announced accordingly
	if(triesBeforeSignalLost >= TRIES_BEFORE_SIGNAL_LOST){
		gpsData -> fix = 0;
		gpsData -> valid = 0;
	}


	// Free allocated memory
	for(int i = 0; i < cnt; i++){
		free(thisData[i]);
	}

	return HAL_OK;
}



