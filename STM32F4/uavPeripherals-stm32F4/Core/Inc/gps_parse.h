/*
 * gps_parse.h
 *
 *  Created on: Jul 18, 2023
 *      Author: ivan
 */

#ifndef INC_GPS_PARSE_H_
#define INC_GPS_PARSE_H_

#include <string.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"

#define NMEA_SENTENCE_BUFFER_LEN	10
#define TRIES_BEFORE_SIGNAL_LOST	5 // How many seconds without a valid sentence before declaring signal lost


// Variables and structures
typedef struct NMEA_DATA {
	uint8_t		valid; // 1 = valid, 0 = data invalid. Are all of these data valid?
	uint8_t		fix; // 1 = fix, 0 = no fix, retreived from NMEA sentence $GGA
    uint8_t		satelliteCount; //number of satellites used in measurement from $GGA
    uint32_t	utcTime; // time in UTC format (4 bytes) from $RMC
    uint32_t	utcDate; // date in UTC format (4 bytes) from $RMC
    int32_t		latE7;	// latitude in 1E7 format (sign indicates North or South) from $GGA
    int32_t		lonE7;  // longitude in 1E7 format (sign indicates East or West) from $GGA
    float		courseGround; // course over ground (float with 0.01 deg. resolution) from $RMC
    float		altitude; // meters over sea level (may be negative in some cases) from $GGA
    float		groundSpeed; // ground speed expressed in meters/second from $RMC
    float		hdop; // horizontal position error from $GGA
    char lastMeasure[10]; // hhmmss.ss UTC of last successful measurement; time read from the GPS module
} GPS;


// Function prototypes
int32_t ddmmmmToDecimalDegrees(uint8_t degrees, float minutes, int8_t side);
int nmeaChecksum(char *nmeaData);
float knotsToMetersSecond(float knots);
HAL_StatusTypeDef nmeaGGA(GPS *gpsData, char *inputString);
HAL_StatusTypeDef nmeaRMC(GPS *gpsData, char *inputString);
HAL_StatusTypeDef nmeaGLL(GPS *gpsData, char *inputString);
HAL_StatusTypeDef nmeaParse(GPS *gpsData, uint8_t *buffer);

#endif /* INC_GPS_PARSE_H_ */
