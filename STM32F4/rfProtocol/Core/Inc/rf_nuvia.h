/*
 * rf_nuvia.h
 *
 *  Created on: Aug 1, 2023
 *      Author: ivan
 */

#ifndef INC_RF_NUVIA_H_
#define INC_RF_NUVIA_H_


/**
 * From DRONES-G Operating Manual, section 8 (Communication Protocol)
 *
 * Nuvia Drones-G Packet Structure
 * | NextB_H | NextB_M | NextB_L | DevAdr | Cmd | Data | ... | Checksum |
 *  --> NextB    - (3 bytes: H, M, L). Represents the packet length. The theoretical maximum packet length is 2^24 - 1
 *  --> DevAdr   - Device Address. Used to distinguish which module (base) should receive the message
 *  --> Cmd      - Command code. Device returns answer with the same code (if command is known)
 *  --> Data     - Data block of variable length that is equal to *NextB - 3*
 *  --> Checksum - Control sum used to verify correctness of received packet
 *
 *
 *  Checksum computation:
 *  	Perform the sum of all bytes in a packet to byte variable (8 bits arithmetic - transfer of last bit (carry) is discarded).
 *  	The result is a two's complement (bit negation of the given number plus one).
 *  	Example:
 *  		- If sum of all bytes is 0x01
 *  		- Negation of 0x01 is then 0xFE
 *  		- Checksum is (0xFE + 1) = 0xFF
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>


#define RF_PAYLOAD_LENGTH_MAX	12300 + 64 + 1 //Based on MLaden's buffer definition at FPGA side + safeguard (64 bytes) + checksum (1 byte)
#define RF_PACKET_LENGTH_MAX	5 + RF_PAYLOAD_LENGTH_MAX //Next_B (3 bytes) + DevAdr (1 byte) + Cmd (1 byte) + Payload&Checksum

#define RF_PACKET_OFFSET_NEXTB		0
#define RF_PACKET_OFFSET_DEVADR		3
#define RF_PACKET_OFFSET_CMD		4
#define RF_PACKET_OFFSET_PAYLOAD	5
#define RF_PACKET_OFFSET_CHECKSUM	-1 // Counting backwards




//! Struct to define the Nuvia RF Packet
typedef struct rfPacket_t rfPacket_t; //<- Structure containing all the elements in the packet
typedef rfPacket_t *rfPacket_handle_t; //<- Each struct of the same type will use different handlers (handler type definition)


//! Public function prototypes
extern rfPacket_handle_t rfInitPacket(uint8_t *payloadBufferPtr, uint32_t payloadLength);
extern void rfResetPacket(rfPacket_handle_t thisPacket);
extern int8_t rfAssemblePackage(rfPacket_handle_t packet, uint8_t devAdr, uint8_t cmd, uint8_t *payload, uint32_t payloadLength);
extern void rfDestroyPacket(rfPacket_handle_t thisPacket);
extern int32_t rfPackageFromRawBytes(uint8_t *rawData, uint32_t rawDataLen, rfPacket_handle_t thisPacket);







#endif /* INC_RF_NUVIA_H_ */


