/*
 * rf_nuvia_testbench.h
 *
 *  Created on: Aug 3, 2023
 *      Author: ivan
 */

#ifndef INC_RF_NUVIA_TESTBENCH_H_
#define INC_RF_NUVIA_TESTBENCH_H_


#include "rf_nuvia.h"


#define RF_TEST_PAYLOAD_LENGTH	10

extern uint8_t	rfTestdestAddr;
extern uint8_t	rfTestcmd;
extern uint8_t	rfTestpayload[RF_TEST_PAYLOAD_LENGTH];



extern rfPacket_handle_t testPacket;



extern int8_t testNewPacket(uint8_t devAdr, uint8_t cmd, uint8_t *payloadData, uint32_t payloadLength);
extern int32_t testPacketDecoding(uint8_t devAdr, uint8_t cmd, uint8_t *payloadData, uint32_t payloadLength);





#endif /* INC_RF_NUVIA_TESTBENCH_H_ */
