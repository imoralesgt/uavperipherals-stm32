/*
 * rf_nuvia_testbench.c
 *
 *  Created on: Aug 3, 2023
 *      Author: ivan
 */


#include "rf_nuvia_testbench.h"
#include "rf_nuvia.h"


//! Some test data
uint8_t	rfTestdestAddr = 0x02;
uint8_t	rfTestcmd = 0x0B;
uint8_t	rfTestpayload[RF_TEST_PAYLOAD_LENGTH] = {
		0x0F,
		0x10,
		0x11,
		0x12,
		0x13,
		0x14,
		0x15,
		0x16,
		0x17,
		0x18
};






// Function prototypes
int8_t testNewPacket(uint8_t devAdr, uint8_t cmd, uint8_t *payloadData, uint32_t payloadLength);
int32_t testPacketDecoding(uint8_t devAdr, uint8_t cmd, uint8_t *payloadData, uint32_t payloadLength);



/*
 * @brief	Tests the low-level Nuvia Packet library functionality to create packets for Tx
 * @param	devAdr: Packet header element - destination address
 * @param	cmd: Packet header element - command
 * @param	payloadData: pointer to data payload in bytes array
 * @param	payloadLength: payload length (without checksum) expressed in number of bytes.
 * @retval	-1 if the payload length is larger than what can be handled, 0 otherwise (no error)
 */
int8_t testNewPacket(uint8_t devAdr, uint8_t cmd, uint8_t *payloadData, uint32_t payloadLength){

	if (payloadLength > RF_PAYLOAD_LENGTH_MAX - 1)
		return -1;

	/*
	 * =======================================
	 * This is how the library should be used.
	 * ------------ START HERE ---------------
	 * =======================================
	 */

	//! Data buffer and packet handler as should be used with this library
	uint8_t *rfBufferTest; //!<- Payload buffer (we initialize it in the rfInitPacket function)
	rfPacket_handle_t rfPacketTestTx; //!<- Packet handler/instance

	int8_t status;

	rfPacketTestTx = rfInitPacket(rfBufferTest, payloadLength);


	//! You can either reuse the same packet instance, o create (and destroy) a new one each time
	//! It's more recommended to simply reuse the existing instance to avoid overhead
	status = rfAssemblePackage(rfPacketTestTx, devAdr, cmd, payloadData, payloadLength);
	rfDestroyPacket(rfPacketTestTx); //!< Always free-up the unused memory

	/*
	 * =======================================
	 * This is how the library should be used.
	 * ------------- END HERE ----------------
	 * =======================================
	 */


	return status;
}


/*
 * @brief	Tests the low-level Nuvia Packet library functionality to create packets of incoming raw data from Rx
 * @param	devAdr: Packet header element - destination address
 * @param	cmd: Packet header element - command
 * @param	payloadData: pointer to data payload in bytes array (synthetic raw data)
 * @param	payloadLength: payload length (without checksum) expressed in number of bytes.
 * @retval	-1 if the payload length is larger than what can be handled, 0 otherwise (no error)
 */
int32_t testPacketDecoding(uint8_t devAdr, uint8_t cmd, uint8_t *payloadData, uint32_t payloadLength){


	/*
	 * Creating synthetic data
	 */
	if (payloadLength > RF_PAYLOAD_LENGTH_MAX - 1)
			return -1;

	//! Variables for the package emulation
	uint8_t thisComputedChecksum = 0x00;
	uint8_t *rawData = malloc(sizeof(uint8_t[payloadLength + 6]));
	uint32_t nBytes = payloadLength + 3;
	uint8_t nBytesB[3] = {
			(nBytes & 0x00FF0000) >> 16,
			(nBytes & 0x0000FF00) >> 8,
			(nBytes & 0x000000FF) >> 0
	};


	volatile int32_t status; //! Used for debugging return value of package decoding



	//! Create a synthetic packet, emulating an incoming frame from UART
	memcpy(rawData + RF_PACKET_OFFSET_NEXTB, &nBytesB[0], 3);
	memcpy(rawData + RF_PACKET_OFFSET_DEVADR, &devAdr, 1);
	memcpy(rawData + RF_PACKET_OFFSET_CMD, &cmd, 1);
	memcpy(rawData + RF_PACKET_OFFSET_PAYLOAD, payloadData, payloadLength);

	//! Compute the checksum corresponding to the emulated packet
	for(uint32_t i = 0; i < payloadLength + 6 - 1; i++)
		thisComputedChecksum |= *(rawData + i);

	//! Append the computed checksum to the raw data
	memcpy(rawData + RF_PACKET_OFFSET_PAYLOAD + payloadLength, &thisComputedChecksum, 1);


	/*
	 * =======================================
	 * This is how the library should be used.
	 * ------------ START HERE ---------------
	 * =======================================
	 */

	//! Destination package initialization
	uint8_t *destinationBuffer = malloc(sizeof(uint8_t[RF_PAYLOAD_LENGTH_MAX]));
	rfPacket_handle_t rfPacketTestRx;
	rfPacketTestRx = rfInitPacket(destinationBuffer, sizeof(destinationBuffer));

	//! Unpacking raw frame
	//! You can either reuse the same packet instance, o create (and destroy) a new one each time
	//! It's more recommended to simply reuse the existing instance to avoid overhead
	status = rfPackageFromRawBytes(rawData, payloadLength + 6, rfPacketTestRx);

	//! Freeing up allocated memory
	rfDestroyPacket(rfPacketTestRx);

	/*
	 * =======================================
	 * This is how the library should be used.
	 * ------------- END HERE ----------------
	 * =======================================
	 */

	free(rawData);



	return 0;
}





