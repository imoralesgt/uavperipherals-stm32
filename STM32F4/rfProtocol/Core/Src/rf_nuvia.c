/*
 * rf_nuvia.c
 *
 *  Created on: Aug 1, 2023
 *      Author: ivan
 *      Description: RF Packet library for base Nuvia data transmission
 */


/**
 * From DRONES-G Operating Manual, section 8 (Communication Protocol)
 *
 * Nuvia Drones-G Packet Structure
 * | NextB_H | NextB_M | NextB_L | DevAdr | Cmd | Data | ... | Checksum |
 *  --> NextB    - (3 bytes: H, M, L). Represents the packet length. The theoretical maximum packet length is 2^24 - 1
 *  --> DevAdr   - Device Address. Used to distinguish which module (base) should receive the message
 *  --> Cmd      - Command code. Device returns answer with the same code (if command is known)
 *  --> Data     - Data block of variable length that is equal to *NextB - 3*
 *  --> Checksum - Control sum used to verify correctness of received packet
 *
 *
 *  Checksum computation:
 *  	Perform the sum of all bytes in a packet to byte variable (8 bits arithmetic - transfer of last bit (carry) is discarded).
 *  	The result is a two's complement (bit negation of the given number plus one).
 *  	Example:
 *  		- If sum of all bytes is 0x01
 *  		- Negation of 0x01 is then 0xFE
 *  		- Checksum is (0xFE + 1) = 0xFF
 */



#include "rf_nuvia.h"


//! RF Packet definition according to Nuvia User's Manual
//! Data and checksum are appended together to allow variable-length payload length
struct rfPacket_t{
	struct nextB{
		uint8_t nextB_H;
		uint8_t nextB_M;
		uint8_t nextB_L;
	}nextBytes; //!<- Packet length NextB (24 bytes)
	uint8_t devAdr; //!<- 1-byte device address
	uint8_t cmd;    //!<- 1-byte RF command
	uint8_t *dataAndChecksum; //<- Variable length RF data + checksum pointer
};




//! Function prototypes
//! 	Private functions
static void rfSetNumberOfBytes(rfPacket_handle_t thisPacket, uint32_t payloadBytes);
static uint32_t rfGetNumberOfBytes(rfPacket_handle_t thisPacket);
static uint8_t rfComputeChecksum(rfPacket_handle_t thisPacket);

//! 	Public functions
rfPacket_handle_t rfInitPacket(uint8_t *payloadBufferPtr, uint32_t payloadLength);
int8_t rfAssemblePackage(rfPacket_handle_t thisPacket, uint8_t devAdr, uint8_t cmd, uint8_t *payload, uint32_t payloadLength);
void rfDestroyPacket(rfPacket_handle_t thisPacket);
void rfResetPacketHeader(rfPacket_handle_t thisPacket);
int32_t rfPackageFromRawBytes(uint8_t *rawData, uint32_t rawDataLen, rfPacket_handle_t thisPacket);


/**
 * @brief	Writes to the specified packet the number of bytes in the nextB_H,_M,_L elements
 * @note	Requires only the number of bytes of the data payload, not counting those from
 * 			DevAdr, Cmd, nor Checksum. The received payloadBytes is then increased by 3 to
 * 			accomplish with the expected NextB format.
 * @param	packet: structure containing the destination packet
 * @param	payloadBytes: number of bytes in the payload (without checksum)
 */


static void rfSetNumberOfBytes(rfPacket_handle_t thisPacket, uint32_t payloadBytes){
	uint8_t nBytesH, nBytesM, nBytesL;

	payloadBytes += 3; //!< Payload bytes + (DevAdr + Cmd + Checksum)

	// Byte masking and shifting
	nBytesH = (payloadBytes & 0x00FF0000) >> 16;
	nBytesM = (payloadBytes & 0x0000FF00) >> 8;
	nBytesL = (payloadBytes & 0x000000FF);

	// Setting computed values to the destination packet
	thisPacket->nextBytes.nextB_H = nBytesH;
	thisPacket->nextBytes.nextB_M = nBytesM;
	thisPacket->nextBytes.nextB_L = nBytesL;

}

/**
 * @brief	Computes the number of bytes stored in NextB struct in packet
 * @param	packet: The packet to analyze
 * @retval	The number of bytes defined in the packet structure
 */
static uint32_t rfGetNumberOfBytes(rfPacket_handle_t thisPacket){
	static uint32_t nBytes;

	//! Number of bytes: NextB_H<<16 + NextB_M<<8 + NextB_L

	nBytes = 0;

	nBytes += (thisPacket->nextBytes.nextB_H) << 16;
	nBytes += (thisPacket->nextBytes.nextB_M) << 8;
	nBytes += (thisPacket->nextBytes.nextB_L);

	return nBytes;
}


/**
 * @brief	Computes checksum of the packet sent as parameter
 * @note	No checksum is expected to be previously included in the packet
 * @param	packet: The packet to analyze (without checksum)
 * @retval	The checksum in 1-byte format
 */
static uint8_t rfComputeChecksum(rfPacket_handle_t thisPacket){

	//! Retrieve the number of bytes to determine the limits on where to loop
	uint32_t nBytes = rfGetNumberOfBytes(thisPacket);

	//! Compute checksum of the packet
	uint8_t checksum =  thisPacket->nextBytes.nextB_H |
						thisPacket->nextBytes.nextB_M |
						thisPacket->nextBytes.nextB_L |
						thisPacket->devAdr |
						thisPacket->cmd;

	for(uint32_t i = 0; i < nBytes - 3; i++)
		checksum |= *((thisPacket->dataAndChecksum) + i);


	//! Two's complement
	checksum ^= 0xFF;
	checksum += 1;

	return checksum;
}


/**
 * @brief	Creates a new RF Packet handler pointed to the payload buffer specified in parameters
 * @note	The payload length is the expected payload depth without the checksum extra byte.
 * 			The buffer is initialized in this same function.
 * @param	payloadBufferPtr: pointer to the buffer where data and checksum are meant to be stored
 * @param	payloadLength: expected length of current packet. It's recommended to be set to RF_PAYLOAD_LENGTH_MAX
 * @retval	Packet handler with assigned buffer. No data is stored yet in the payload.
 */

rfPacket_handle_t rfInitPacket(uint8_t *payloadBufferPtr, uint32_t payloadLength){
	rfPacket_handle_t thisPacket = (rfPacket_handle_t) malloc(sizeof(rfPacket_t));

	payloadBufferPtr = (uint8_t *) malloc(payloadLength + 1);
	memset(payloadBufferPtr, 0x00, payloadLength + 1);

	thisPacket->dataAndChecksum = payloadBufferPtr;
	rfResetPacketHeader(thisPacket); //<! Clears the contents of any previous data in the current memory locations of the new packet header

	return thisPacket;
}



/**
 * @brief	Used in case the packet is not required anymore. Frees up the allocated memory
 * @param	thisPacket: The packet we want to destroy
 */
void rfDestroyPacket(rfPacket_handle_t thisPacket){
	free(thisPacket->dataAndChecksum);
	free(thisPacket);
}

/**
 * @brief	Cleans the contents of the packet header without unlinking the data buffer pointer
 * @param	packet: the Packet to reset header from
 */
void rfResetPacketHeader(rfPacket_handle_t thisPacket){
	memset(&(thisPacket->nextBytes), 0x00, 3); // The three-byte NBytes struct
	thisPacket->devAdr = 0x00;
	thisPacket->cmd = 0x00;
}



/**
 * @brief	Assembles the complete packet using the header and payload information. Computes checksum here.
 * @param	rfPacket: pointer to the struct on which the packet will be assembled
 * @param	devAdr: packet header element - device address
 * @param	cmd: packet header element - command
 * @param	payload: the data to be stored in the packet
 * @param	payloadLength: the number of bytes of the current payload
 * @retval	Returns -1 if the payload is too big to fit into the packet, 0 otherwise
 */

int8_t rfAssemblePackage(rfPacket_handle_t thisPacket, uint8_t devAdr, uint8_t cmd, uint8_t *payload, uint32_t payloadLength){

	if (payloadLength > RF_PAYLOAD_LENGTH_MAX - 1)
		return -1;

	uint8_t thisChecksum = 0x00;

	// Writes into the current packet the NextB value of the header
	rfSetNumberOfBytes(thisPacket, payloadLength);

	// The remaining elements in the header
	thisPacket->devAdr = devAdr;
	thisPacket->cmd = cmd;

	// Copy the data payload (without checksum) into the destination packet
	memcpy((thisPacket->dataAndChecksum), payload, payloadLength);

	// Compute and append the checksum into the packet payload
	thisChecksum = rfComputeChecksum(thisPacket);
	*((thisPacket -> dataAndChecksum) + payloadLength) = thisChecksum;

	return 0;
}

/**
 * @brief	Gets a raw bytes buffer (from UART, for example) and creates the packet structure
 * @param	rawData: pointer to the raw bytes to be processed
 * @param	rawDataLen: number of bytes received/stored in the rawData array
 * @param	thisPacket: Receives and INITIALIZED packet handler. If no buffer has been assigned, error -2 is returned.
 * 						The decoded packet is stored here.
 *
 * @retval	In case of successful checksum verification: the number payload length is returned (NBytes - 3)
 * 			Otherwise returns -1 if the checksum fails.
 * 			If the provided packet is not initialized, returns error -2.
 */

int32_t rfPackageFromRawBytes(uint8_t *rawData, uint32_t rawDataLen, rfPacket_handle_t thisPacket){


	if(thisPacket -> dataAndChecksum == NULL)
		return -2;

	uint32_t payloadLen = rawDataLen - 6; //!< NextB (3 bytes) + DevAdr (1 byte) + Cmd (1 byte) + Payload + Checksum (1 byte) = 6 extra bytes

	//! Compute the received checksum
	uint8_t thisComputedChecksum = 0x00;
	for(uint32_t i = 0; i < rawDataLen - 1; i++)
		thisComputedChecksum |= *(rawData + i);

	//! If the checksum does not coincides, return error and do nothing else
	if(thisComputedChecksum != *(rawData + rawDataLen - 1))
		return -1;

	// Copy the NextB field (3 bytes)
	memcpy(&(thisPacket -> nextBytes), rawData + RF_PACKET_OFFSET_NEXTB, 3);

	// Copy the remaining header elements (DevAdr and Cmd: 2 bytes)
	memcpy(&(thisPacket -> devAdr), rawData + RF_PACKET_OFFSET_DEVADR, 2);

	// Dump the complete payload into the destination packet
	memcpy(thisPacket -> dataAndChecksum, rawData + RF_PACKET_OFFSET_PAYLOAD, payloadLen);

	// Get the received checksum and store it in the destination packet
	*((thisPacket -> dataAndChecksum) + payloadLen) = thisComputedChecksum;


	return payloadLen;

}

