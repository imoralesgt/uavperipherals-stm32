/*
 * rf_nuvia_comm.c
 *
 *  Created on: Aug 4, 2023
 *      Author: ivan
 *      Description: High-level communication protocol implemented atop of base RF Nuvia Packet library (rf_nuvia)
 */


#include "rf_nuvia_comm.h"

//! Get Spectrum command packet data structure (payload)
struct cmGetSpectrum_t{
	uint8_t			b00_status;
	uint16_t		b01_lld;
	uint16_t		b03_uld;
	uint32_t		b05_realTime;
	uint32_t		b09_liveTime;
	uint16_t		b0D_convGain;
	uint32_t		b0F_timestamp;
	cmSpectrumBuf_t	b14_spectrum; //!< Pointer must be initialized beforehand
};

//! Module (individual detector) struct definition used within cmGetPodMeas payload struct
struct nuviaDeviceModule_t{
	uint8_t		b00_deviceType;
	uint8_t		b01_deviceAddr;
	uint8_t		b02_flags;
	uint16_t	b03_highVoltage;
	uint32_t	b05_countRate;
	float		b09_doseRate;
};

//! UTC Date structe definition used within cmGetPodMeas payload struct
struct utcDate_t{
	uint8_t		b00_DUMMY_NOT_USE;
	uint8_t		b01_utcDD;
	uint8_t		b02_utcMM;
	uint8_t		b03_utcYY;
};

//! UTC Time structe definition used within cmGetPodMeas payload struct
struct utcTime_t{
	uint8_t		b00_DUMMY_NOT_USE;
	uint8_t		b01_utcHH;
	uint8_t		b02_utcMM;
	uint8_t		b03_utcSS;
};

//! Get Pod Measurement command packet data structure (payload)
struct cmGetPodMeas_t{

	uint8_t				b00_packetVersion;
	uint16_t			b01_batVoltage;
	uint16_t			b03_batCurrent;
	uint8_t				b05_gpsFix;
	uint8_t				b06_numSatellites;
	int32_t				b07_latitude1E7; // User's manual indicates UNSIGNED, but makes no sense
	int32_t				b0B_longitude1E7; // Same as longitude, makes no sense to be unsigned int
	uint16_t			b0F_gpsAltitude;
	struct utcDate_t	b11_utcDate;
	struct utcTime_t	b16_utcTime;
	uint16_t			b19_atmPressure;
	uint16_t			b1B_barPressure;
	uint16_t			b1D_relBarAltitude;
	uint16_t			b1F_laserAltitude;
	uint16_t			b21_ambientTemp;
	uint16_t			b23_relativeHum;
	uint32_t			b25_measurementStamp;

	//! Elements related to individual onboard detectors
	struct nuviaDeviceModule_t	b29_module0;
	struct nuviaDeviceModule_t	b36_module1;
	struct nuviaDeviceModule_t	b43_module2;
	struct nuviaDeviceModule_t	b50_module3;
	struct nuviaDeviceModule_t	b5D_module4;
};



//! GPS Structure used in NMEA library
typedef struct NMEA_DATA{
	uint8_t		valid; // 1 = valid, 0 = data invalid. Are all of these data valid?
	uint8_t		fix; // 1 = fix, 0 = no fix, retreived from NMEA sentence $GGA
    uint8_t		satelliteCount; //number of satellites used in measurement from $GGA
    uint32_t	utcTime; // time in UTC format (4 bytes) from $RMC
    uint32_t	utcDate; // date in UTC format (4 bytes) from $RMC
    int32_t		latE7;	// latitude in 1E7 format (sign indicates North or South) from $GGA
    int32_t		lonE7;  // longitude in 1E7 format (sign indicates East or West) from $GGA
    float		courseGround; // course over ground (float with 0.01 deg. resolution) from $RMC
    float		altitude; // meters over sea level (may be negative in some cases) from $GGA
    float		groundSpeed; // ground speed expressed in meters/second from $RMC
    float		hdop; // horizontal position error from $GGA
    char lastMeasure[10]; // hhmmss.ss UTC of last successful measurement; time read from the GPS module
}gps;



//! Global variables in library
//uint16_t *rfCommAltimeter;
//uint8_t *rfCommSpectrumBuffer;
//struct NMEA_DATA *rfCommGps;



/**
 * @brief	Initialize external buffers that feed the RF Comm data. Pointer assigned to external buffers for easy manipulation
 * @param	gps: struct of external GPS API containing NMEA data already parsed from the last update
 * @param	altimeter: altitude from laser ragefinder in CENTIMETER units, pointer to actual data source
 * @param 	spectrum: pointer to spectrum buffer from FPGA
 * @param	spectrumBuffSize: expected number of channels of the spectrum
 * @retval	returns 0 if no errors, -1 otherwise
 */
int8_t rfCommInitBuffers(struct NMEA_DATA *gps, uint16_t *altimeter, uint32_t *spectrum, uint32_t spectrumBuffSize){

	//! Assign external buffers to local pointers
	rfCommAltimeter = altimeter;
	rfCommSpectrumBuffer = spectrum;
	rfCommGps = gps;

	//! Initializing the cmGetPodMeas command counter, updated each time an RF packet with cmGetPodMeas is requested
	cmGetPodMeasCounter = 0;

	//! Initializing buffer values to avoid spurious data
	*rfCommAltimeter = 0;
	memset(rfCommSpectrumBuffer, 0x00, spectrumBuffSize/sizeof(uint32_t));
	memset(rfCommGps, 0x00, sizeof(*gps));

	return 0;
}


/**
 * @brief	Updates the Rf Tx structure with the latest data from sensors and spectrum
 * @note	Unit conversion is carried out here, to match the expected RF format with that
 * 			from the incoming data from the sensors
 * @param	None
 * @retval	Returns 0 if no error, -1 if GPS error, -2 if altitude error
 */
int8_t updateRfTxBuffer(void){
	static uint16_t gpsAlt, laserAlt;

	//! Update measurement counter
	cmGetPodMeasCounter++;

	//! Convert the latest data from sensors with scaling factors
	gpsAlt = (uint16_t) (rfCommGps->altitude)*SCALE_FACTOR_GPS_ALT_TO_RF; //!< GPS altitude in decimeters
	laserAlt = (uint16_t) (*(rfCommAltimeter))*SCALE_FACTOR_ALTIMETER_TO_RF; //!< Laser rangefinder altitude in decimeters




	return 0;

}




